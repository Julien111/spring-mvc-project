package com.luv2code.spring.mvc.model;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.luv2code.spring.mvc.validation.CourseCode;

public class Customer {
	
	@NotNull(message="Champ obligatoire")
	@Size(min=1, message="Le champ doit contenir au moins un caractère")
	private String lastName;
	
	private String firstName;	
	
	@NotNull(message="Champ requis")
	@Min(value=0, message="Plus grand que zéro")
	@Max(value=10, message="Le nombre doit être inférieur à 10.")
	private Integer freePass;
	
	@Pattern(regexp="^[a-zA-Z0-9]{5}", message="Seulement 5 caractères")
	private String postalCode;
	
	@CourseCode(value={"JIV", "TT"}, message="Le code doit commencer par TT ou JIV")
	private String courseCode;

	public Customer() {
		super();		
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Integer getFreePass() {
		return freePass;
	}

	public void setFreePass(Integer freePass) {
		this.freePass = freePass;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCourseCode() {
		return courseCode;
	}

	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}	

}
