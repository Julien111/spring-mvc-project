package com.luv2code.spring.mvc;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/hello")
public class HelloFormController {

	// afficher le formulaire
	@RequestMapping("/showHelloForm")
	public String helloPage() {
		return "hello-form";
	}

	// afficher le résultat
	@RequestMapping("/resultPage")
	public String resultPage() {		
		return "result-form";
	}
	
	//test post mapping plus request and model
	
	@PostMapping("/displayName")
	public String displayName(HttpServletRequest request, Model model) {
		
		String nameStudent = request.getParameter("studentName");
		
		//convert data
		String upper = "Your name is " + nameStudent.toUpperCase();
		
		//add attribute
		
		model.addAttribute("message", upper);		
		
		return "result-form";
	}
	
	//test request parameter
	
	@PostMapping("/displayNameTwo")
	public String displayNameTwo(@RequestParam("studentName") String theName, Model model) {		
				
		//convert data
		String upper = "Your name is (v3) " + theName.toUpperCase();
		
		//add attribute
		
		model.addAttribute("message", upper);		
		
		return "result-form";
	}

}
