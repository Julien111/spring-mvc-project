package com.luv2code.spring.mvc.validation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.luv2code.spring.mvc.validation.impl.CourseCodeConstraintValidator;

//@target ou l'annotation peut être utilisé méthode ou champ.
//@Retention combien de temps l'annotation sera conservée et utilisée

@Constraint(validatedBy = CourseCodeConstraintValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME) 
public @interface CourseCode {
	
	//course code
	
	public String[] value() default {"DA"};
	
	//message erreur
	
	public String message() default "Le code n'est pas valide";
	
	//groupe par défaut
	
	public Class<?>[] groups() default {};
	
	//définir le payload
	
	public Class<? extends Payload>[] payload() default {};

}
