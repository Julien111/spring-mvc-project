package com.luv2code.spring.mvc.validation.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.luv2code.spring.mvc.validation.CourseCode;

public class CourseCodeConstraintValidator implements ConstraintValidator<CourseCode, String>{
	
	private String[] coursePrefixes;	
	
	@Override
	public void initialize(CourseCode theCode) {
		coursePrefixes = theCode.value();
	}
	
	//theCode le code que l'utilisateur a rentré et validator context est le message d'erreur si il y a une erreur

	@Override
	public boolean isValid(String theCode, ConstraintValidatorContext theConstraintValidator) {
		boolean result = false;
		if(theCode != null) {
			for(String code : coursePrefixes) {
				result = theCode.startsWith(code);
				if(result) {
					break;
				}
			}
		}
		else {
			result = true;
		}
		
		return result;
	}

}
