package com.luv2code.spring.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.luv2code.spring.mvc.model.Student;

@Controller
@RequestMapping("/student")
public class StudentController {
	
	@GetMapping("/showForm")
	public String showForm(Model theModel) {		
		Student theStudent = new Student();
		theModel.addAttribute("student", theStudent);		
		return "student-form";
	}
	
	/**
	 * Méthode de gestion de formulaire
	 * @param theStudent
	 * @return String
	 */
	@PostMapping("/processFormStudent")
	public String processFormStudent(@ModelAttribute("student") Student theStudent, Model theModel) {
		System.out.println("Prénom " + theStudent.getFirstName());
		//on récupère les informations
		theModel.addAttribute("student", theStudent);		
		return "student-confirmation";
	}
}
