<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Student confirmation</title>
</head>
<body>

<h2>Les informations sur l'�tudiant</h2>
The informations is confirmed : ${student.firstName} and ${student.lastName}.
Votre pays : ${ student.country }.
<br>
Votre langage pr�f�r� : ${student.favouriteLanguage}.
<br>
<ul>
	<c:forEach var="os" items="${ student.operatingSystem }">
		<li><c:out value="${os}"></c:out></li>
	</c:forEach>
</ul>
</body>
</html>