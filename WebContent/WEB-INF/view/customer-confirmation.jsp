<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Customer Confirmation</title>
</head>
<body>
	<h1>Confirmation</h1>
	<p>Le client est confirm� avec le nom ${customer.lastName}</p>
	<p>Code : ${customer.freePass}</p>
	<p>Code postal : ${customer.postalCode}</p>
	<p>Le code du cours : ${ customer.courseCode } </p> 
</body>
</html>