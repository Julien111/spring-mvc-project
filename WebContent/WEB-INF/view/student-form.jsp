<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h2>Formulaire d'inscription</h2>

<form:form action="processFormStudent" modelAttribute="student">
	<label>First name</label>
	<form:input path="firstName"/>
	<br>
	<label>Last name</label>
	<form:input path="lastName"/>
	<br>	
	<form:select path="country">
		<form:options items="${ student.countryOptions }"></form:options>		
	</form:select>
	<br>	
	<label>Favorite language : </label>
	Java<form:radiobutton path="favouriteLanguage" value="Java"/>
	PHP<form:radiobutton path="favouriteLanguage" value="PHP"/>
	c#<form:radiobutton path="favouriteLanguage" value="C#"/>
	Ruby<form:radiobutton path="favouriteLanguage" value="Ruby"/>
	<br>
	<label>Favorite OS : </label>
	<form:checkbox path="operatingSystem" value="Linux" label="Linux"/>
	<form:checkbox path="operatingSystem" value="Windows" label="Windows"/>
	<form:checkbox path="operatingSystem" value="Mac OS" label="Mac OS"/>
	<br>
	<input type="submit" value="Submit" />
</form:form>

</body>
</html>