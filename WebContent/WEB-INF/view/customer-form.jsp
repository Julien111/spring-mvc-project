<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Customer register form</title>
</head>
<body>
<h2>Formulaire</h2>
<p>Champ obligatoire *</p>
	<form:form action="processForm" modelAttribute="customer">
      <label>Pr�nom</label>	
	  <form:input path="firstName" /><br>
	   <label>Nom(*)</label>	
	  <form:input path="lastName" />
	  <form:errors path="lastName" cssClass="error" /> <br>	
	  <label>Code gratuit (*)</label>	
	  <form:input path="freePass" />
	  <form:errors path="freePass" cssClass="error" /><br>
	  <label>Code postal</label>
	  <form:input path="postalCode" />
	  <form:errors path="postalCode" cssClass="error" /><br>
	  <label>Code du cours</label>
	  <form:input path="courseCode" />
	  <form:errors path="courseCode" cssClass="error" /><br>
	  <input type="submit" value="Valider" />	
	</form:form>	
</body>
</html>